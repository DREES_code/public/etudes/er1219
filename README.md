# ER1219
Ce dossier fournit les programmes permettant de reproduire les bases de données, illustrations et chiffres de l'Études et résultats N°&nbsp;1219 de la DREES : [« En France, les AVC parmi les personnes les plus modestes sont plus fréquents, plus graves et moins souvent pris en charge en unité neuro-vasculaire »](https://drees.solidarites-sante.gouv.fr/publications-communique-de-presse/etudes-et-resultats/en-france-les-avc-sont-plus-frequents-plus), 09/02/2022. Il contient également la [publication](er1219_1.pdf) et son [tableur](er1219_0.xls).

Plusieurs programmes sont mis à disposition :

- *01_production_AVC.R* (17 minutes avec 40 cœurs de processeur) produit la base de données principale `EDPavc` en utilisant
    - les données du SNDS
    - les données de l'EDP, synthétisées sous la forme d'une table `individu_S`
    - des données concernant les lits d'UNV et de SSR par département, extraites du portail PMSI de l'ATIH et issues de la SAE, *Lit UNV 2014-2017.xlsx*, *Lit SSR 2014-2017 par type.xlsx*
    - les estimations localisées de population, déjà préparées, *elp.RData*
    - une table des pondérations des composantes du score de charlson *charlson_quan_2005.csv*
- *02_chargement.R* (4 minutes avec 40 cœurs de processeur) la charge et produit les objets finaux `popuTot`, `popuAVC`, `popuAVCvie`, `popuSSR`, `popuSSR_ssReadm`, `popuIsche`, `popuHemo`, `popuSSRneuro` 
- *03_modeles_auto_estimation.R* (11 secondes avec 40 cœurs de processeur) estime une cinquantaine de modèles logistiques, pas tous présentés dans l'ER, en utilisant les spécifications fournies par *03_modeles_specif.xlsx* 
- *04_resultats_ER.R* (1 minute 30 si *02_chargement.R* a déjà été exécuté) qui produit toutes les statistiques présentées dans l'ER ainsi que le tableur l'accompagnant 

*Réplicabilité :* ce code source ne permet pas à lui seul de répliquer la résultat dans la mesure où il s'appuie sur une table de synthèse des données de l'EDP, appelée `individu_S` et produite par la Drees. Le code source de la chaîne de production sera partagé ultérieurement.
En revanche, le code source de *01_production_AVC.R* permet de comprendre le repérage des AVC et la construction des épisodes de soins.
*04_resultats_ER.R* permet de vérifier le calcul des résultats, notamment la standardisation directe réalisée pour les taux de survenue.

Source de données : EDP-Santé, appariement de l'Échantillon Démographique Permanent (INSEE) avec les données du SNDS, millésime 2017. Traitements : Drees. Ces données ne sont pas ouvertes à des demandes extérieures pour le moment.
Lien vers la documentation de l’EDP : https://utiledp.site.ined.fr/

Les programmes ont été exécutés pour la dernière fois avec le logiciel R version 3.6.0, le 26/01/2022.

La Direction de la recherche, des études, de l’évaluation et des statistiques (Drees) est une direction de l’administration centrale des ministères sanitaires et sociaux. https://drees.solidarites-sante.gouv.fr/etudes-et-statistiques/la-drees/qui-sommes-nous/ 

