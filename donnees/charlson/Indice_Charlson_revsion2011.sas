data essai;
set SSR09;

charlson_age=0;
charlson_1=0;
charlson_2=0;
charlson_3=0;
charlson_4=0;
charlson_5=0;
charlson_6=0;
charlson_7=0;
charlson_8=0;
charlson_9=0;
charlson_10=0;
charlson_11=0;
charlson_12=0;
charlson_13=0;
charlson_14=0;
charlson_15=0;
charlson_16=0;
charlson_17=0;


if ((agemco GE 50) and (agemco LT 60))then charlson_age=1;
if ((agemco GE 60) and (agemco LT 70))then charlson_age=2;
if ((agemco GE 70) and (agemco LT 80))then charlson_age=3;
if ((agemco GE 80) and (agemco LT 90))then charlson_age=4;
if (agemco GE 90) then charlson_age=5;

diagasre1=substr (diaga1,1,3);
diagasre2=substr (diaga2,1,3);
diagasre3=substr (diaga3,1,3);
diagasre4=substr (diaga4,1,3);
diagasre5=substr (diaga5,1,3);
diagasre6=substr (diaga6,1,3);
diagasre7=substr (diaga7,1,3);
diagasre8=substr (diaga8,1,3);
diagasre9=substr (diaga9,1,3);
diagasre10=substr (diaga10,1,3);
diagasre11=substr (diaga11,1,3);
diagasre12=substr (diaga12,1,3);
diagasre13=substr (diaga13,1,3);
diagasre14=substr (diaga14,1,3);
diagasre15=substr (diaga15,1,3);
diagasre16=substr (diaga16,1,3);
diagasre17=substr (diaga17,1,3);
diagasre18=substr (diaga18,1,3);
diagasre19=substr (diaga19,1,3);
diagasre20=substr (diaga20,1,3);


array diagasre{20} diagasre1-diagasre20;
array diagas{20} diaga1-diaga20;


do count= 1 to 20; 
if ((diagasre{count} in ("I21","I22"))or (diagas{count} eq "I252"))then charlson_1=0;
if ((diagas{count} in ("I099", "I110","I130","I132","I255", "I420","I425","I426","I427","I428","I429","P290"))
     or (diagasre{count} in ("I43","I50"))) then charlson_2=2;
if ((diagasre{count} in ("I70","I71"))or (diagas{count} in ("I731","I738","I739","I771","I790","I792",
                                                            "K551","K558","K559","Z958","Z959")))then charlson_3=0; 
if ((diagasre{count} in ("G45","G46", "I60", "I61", "I62", "I63", "I64", "I65", "I66", "I67", "I68", "I69"))
      or (diagas{count} eq "H340"))then charlson_4=0;
if ((diagasre{count} in ("F00","F01", "F02","F03","G30"))or (diagas{count} in ("F051","G311")))then charlson_5=2; 
if ((diagasre{count} in ("J40","J41","J42","J43","J44","J45","J46","J47","J60","J61","J62","J63","J64","J65","J66","J67"))
     or (diagas{count} in ("I278","I279","J684","J701","J703")))then charlson_6=1; 
if ((diagasre{count} in ("M05","M06","M32","M33","M34"))or (diagas{count} in ("M315","M351","M353","M360")))then charlson_7=1; 
if (diagasre{count} in ("K25","K26","K27","K28"))then charlson_8=0; 

if  (diagas{count} in ("E102","E103","E104","E105","E107","E112","E113","E114","E115","E117","E122","E123","E124","E125","E127",
                       "E132","E133","E134","E135","E137","E142","E143","E144","E145","E147" ))then charlson_11=1; 
if ( (charlson_11=0) and (diagas{count} in ("E100","E101","E106","E108","E109","E110", "E111","E116", "E118","E119","E120", "E121","E126", "E128","E129",
"E130", "E131","E136", "E138","E139","E140", "E141","E146", "E148","E149")))then charlson_10=0; 

if ((diagasre{count} in ("G81","G82"))or (diagas{count} in ("G041","G114","G801","G802","G830","G831","G832","G833","G834","G839")))
                                          then charlson_12=2; 

if ((diagasre{count} in ("N18","N19"))or (diagas{count} in ("I120","I131","N032","N033","N034","N035","N036","N037",
"N052","N053", "N054","N055","N056","N057","N250","Z490","Z491","Z492","Z940","Z992","Z992+0","Z992+1","Z992+8")))then charlson_13=1; 


if  (diagas{count} in ("I850","I859","I864","I982","K704","K711","K721","K729","K765","K766","K767"))
                                          then charlson_15=4; 
if ((charlson_15=0) and((diagasre{count} in ("B18","K73","K74"))or (diagas{count} in ("K700","K701", "K702", "K703", "K709","K713","K714","K715",
                                                               "K717","K760", "K762","K763","K764","K768","K769","Z944"))))
                                             then charlson_9=2; 
if (diagasre{count} in ("C77","C78","C79","C80"))
                                          then charlson_16=6; 
if ( (charlson_16=0)and (diagasre{count} in ("C00","C01","C02","C03","C04", "C05", "C06","C07","C08","C09",
                        "C10","C11","C12","C13","C14", "C15", "C16","C17","C18","C19", 
	                    "C20","C21","C22","C23","C24", "C25", "C26",
                        "C30","C31","C32","C33","C34","C37","C38","C39"
                        "C40","C41","C43", "C45", "C46","C47","C48","C49",
                        "C50","C51","C52","C53","C54", "C55", "C56","C57","C58",
                        "C60","C61","C62","C63","C64", "C65", "C66","C67","C68","C69",
                        "C70","C71","C72","C73","C74", "C75", "C76",
                        "C81","C82","C83","C84", "C85", "C88",
                        "C90","C91","C92","C93","C94", "C95", "C96","C97")))
                                          then charlson_14=2; 
if (diagasre{count} in ("B20","B21","B22","B24"))
                                          then charlson_17=4; 


end;

charlson_morb_age= charlson_age + charlson_1 + charlson_2 + charlson_3 + charlson_4 +charlson_5 + charlson_6 + charlson_7
                                + charlson_8 + charlson_9 + charlson_10 + charlson_11 +charlson_12 + charlson_13 + charlson_14
								+ charlson_15 + charlson_16 + charlson_17 ;  
charlson_morb=  charlson_1 + charlson_2 + charlson_3 + charlson_4 +charlson_5 + charlson_6 + charlson_7
              + charlson_8 + charlson_9 + charlson_10 + charlson_11 +charlson_12 + charlson_13 + charlson_14
				+ charlson_15 + charlson_16 + charlson_17 ;
charlson_morb2=  charlson_1 + charlson_2 + charlson_3  +charlson_5 + charlson_6 + charlson_7
              + charlson_8 + charlson_9 + charlson_10 + charlson_11 +charlson_12 + charlson_13 + charlson_14
				+ charlson_15 + charlson_16 + charlson_17 ;  /*sans charlson_4 mal cerebro-vasculaire */	

run;

/*50 v�rifi�s, c'est OK*/
data essai2;
set essai;
if 
